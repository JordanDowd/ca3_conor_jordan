//Changes by Kevin
#include <RoboCatServerPCH.h>



//uncomment this when you begin working on the server

bool Server::StaticInit()
{
	ShadowFactory::StaticInit();
	ConnectionDetails::StaticInit();
	PersistantPlayerSprites::StaticInit();

	sInstance.reset( new Server() );
	return true;
}

Server::Server()
{

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnServer::StaticCreate );

	InitNetworkManager();

	TimeBetweenBulletSpawns = 2.f;
	TimeBetweenHealthSpawns = 7.f;
	SpawnTimeBullets = 0.f;
	SpawnTimeHealth = 0.f;
	PlayerCount = 0;
	PlayersNeeded = 2;
	//NetworkManagerServer::sInstance->SetDropPacketChance( 0.8f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.1f );

	m_spawnPoints = {
		Vector3(80, 80, 0),		//top-left
		Vector3(420, 80, 0),	//top-middle-1
		Vector3(780, 80, 0),	//top-middle-2
		Vector3(1120, 80, 0),	//top-right
		Vector3(80, 400, 0),	//middle-left
		Vector3(1120, 400, 0),	//middle-right
		Vector3(80, 720, 0),	//bottom-left
		Vector3(420, 720, 0),	//bottom-middle-1
		Vector3(780, 720, 0),	//bottom-middle-2
		Vector3(1120, 720, 0)	//bottom-right
	};

}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	uint16_t port = ConnectionDetails::sInstance->GetServerPort();
	return NetworkManagerServer::StaticInit( port );
}


namespace
{
	
	void CreateRandomBullets( int inMouseCount )
	{
		Vector3 mouseMin( 60.f, 60.f, 0.f );
		Vector3 mouseMax(1170.f, 770.f, 0.f );
		GameObjectPtr go;
		Vector3 mouseLocation;
		float size = 32;
		//make a mouse somewhere- where will these come from?
		for (int i = 0; i < inMouseCount; ++i)
		{
			go = GameObjectRegistry::sInstance->CreateGameObject('YARN');
			do {
				mouseLocation = RoboMath::GetRandomVector(mouseMin, mouseMax);
			} while (ShadowFactory::sInstance->doesCollideWithWorld(sf::FloatRect(mouseLocation.mX - (size / 2), mouseLocation.mY - (size / 2), size, size)));
			go->SetLocation(mouseLocation);
			
		}
	}

	void CreateRandomHealth(int inMouseCount)
	{
		Vector3 mouseMin(0.f, 0.f, 0.f);
		Vector3 mouseMax(1920.f, 1280.f, 0.f);
		GameObjectPtr go;
		Vector3 mouseLocation;
		float size = 32;
		//make a mouse somewhere- where will these come from?
		for (int i = 0; i < inMouseCount; ++i)
		{
			go = GameObjectRegistry::sInstance->CreateGameObject('MOUS');
			do {
				mouseLocation = RoboMath::GetRandomVector(mouseMin, mouseMax);
			} while (ShadowFactory::sInstance->doesCollideWithWorld(sf::FloatRect(mouseLocation.mX - (size / 2), mouseLocation.mY - (size / 2), size, size)));
			go->SetLocation(mouseLocation);

		}
	}


}
void Server::PickupUpdate()
{
	if (PlayerCount >= PlayersNeeded) //4 players to start the bullets and health
	{
		float time = Timing::sInstance.GetFrameStartTime();
		if (Timing::sInstance.GetFrameStartTime() > SpawnTimeBullets)
		{
			SpawnTimeBullets = time + TimeBetweenBulletSpawns;
			CreateRandomBullets(1);
		}

		if (Timing::sInstance.GetFrameStartTime() > SpawnTimeHealth)
		{
			SpawnTimeHealth = time + TimeBetweenHealthSpawns;
			CreateRandomHealth(1);
		}
	}
}

void Server::SetupWorld()
{
	//spawn some random mice
	//CreateRandomMice( 1 );
}

void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnCats();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
	
	this->PickupUpdate();
}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{
	
	int playerId = inClientProxy->GetPlayerId();
	
	ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	SpawnCatForPlayer( playerId );

}

void Server::SpawnCatForPlayer( int inPlayerId )
{
	RoboCatPtr cat = std::static_pointer_cast< RoboCat >( GameObjectRegistry::sInstance->CreateGameObject( 'RCAT' ) );
	cat->SetColor( ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	cat->SetPlayerId( inPlayerId );
	
	// Pick one of a few random locations.
	int randomIndex = rand() % m_spawnPoints.size();
	cat->SetLocation( m_spawnPoints[randomIndex] );

	PlayerCount++;

}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	ScoreBoardManager::sInstance->RemoveEntry( playerId );
	RoboCatPtr cat = GetCatForPlayer( playerId );
	if( cat )
	{
		cat->SetDoesWantToDie( true );
	}
}

RoboCatPtr Server::GetCatForPlayer( int inPlayerId )
{
	//run through the objects till we find the cat...
	//it would be nice if we kept a pointer to the cat on the clientproxy
	//but then we'd have to clean it up when the cat died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];
		RoboCat* cat = go->GetAsCat();
		if( cat && cat->GetPlayerId() == inPlayerId )
		{
			return std::static_pointer_cast< RoboCat >( go );
		}
	}

	return nullptr;

}
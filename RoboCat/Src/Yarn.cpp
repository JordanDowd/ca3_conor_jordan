#include <RoboCatPCH.h>

Yarn::Yarn() :
	mMuzzleSpeed( 250.f ),
	mVelocity(),
	mPlayerId( 0 )
{
	SetScale( GetScale() * .05f );
	SetCollisionRadius( 25.f );
	
	int rand1 = rand() % 3 + (-1);
	int rand2 = rand() % 3 + (-1);
	if (rand1 == 0 && rand2 == 0)
	{
		rand1 = -1;
		rand2 = 1;
	}
	mVelocity = (Vector3(rand1, rand2, 0)* mMuzzleSpeed);
}



uint32_t Yarn::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const 
{
	uint32_t writtenState = 0;

	if( inDirtyState & EYRS_Pose )
	{
		inOutputStream.Write( (bool)true );

		Vector3 location = GetLocation();
		inOutputStream.Write( location.mX );
		inOutputStream.Write( location.mY );

		Vector3 velocity = GetVelocity();
		inOutputStream.Write( velocity.mX );
		inOutputStream.Write( velocity.mY );

		inOutputStream.Write( GetRotation() );

		writtenState |= EYRS_Pose;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if( inDirtyState & EYRS_Color )
	{
		inOutputStream.Write( (bool)true );

		inOutputStream.Write( GetColor() );

		writtenState |= EYRS_Color;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if( inDirtyState & EYRS_PlayerId )
	{
		inOutputStream.Write( (bool)true );

		inOutputStream.Write( mPlayerId, 8 );

		writtenState |= EYRS_PlayerId;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}




	return writtenState;
}



bool Yarn::HandleCollisionWithCat( RoboCat* inCat )
{
	( void ) inCat;

	//you hit a cat, so look like you hit a cat

	return false;
}

//Kevin
//Rather than using the normalised velocity of the player to get the direction the yarn now 
//finds the direction the player is pointing by using the players rotation.
void Yarn::InitFromShooter( RoboCat* inShooter )
{
	SetColor( inShooter->GetColor() );
	SetPlayerId( inShooter->GetPlayerId() );
	

	Vector3 forward = inShooter->GetForwardVector();
	Vector3 vel = inShooter->GetVelocity();
	auto normVel = thor::unitVector(sf::Vector2f(vel.mX, vel.mY));
	sf::Vector2f temp = sf::Vector2f(0, -1);
	thor::rotate(temp, inShooter->GetRotation());

	//SetVelocity(Vector3(normVel.x, normVel.y, 0) * mMuzzleSpeed);
	
	SetVelocity(Vector3(temp.x, temp.y, 0) * mMuzzleSpeed);
	SetLocation( inShooter->GetLocation() /*+ Vector3(temp.x,temp.y,0) * 0.55f*/ );

	SetRotation( inShooter->GetRotation() );
}

void Yarn::Update()
{
	
	float deltaTime = Timing::sInstance.GetDeltaTime();

	SetLocation( GetLocation() + mVelocity * deltaTime );
	
	sf::FloatRect yarn(GetLocation().mX - (25 / 2), GetLocation().mY - (25 / 2), 25, 25);
	int side = ShadowFactory::sInstance->doesExitWithMapBoundsSide(yarn);
	
	if (side != 4)
	{
		Vector3 vel = GetVelocity();
		auto normVel = thor::unitVector(sf::Vector2f(vel.mX, vel.mY));
		if (side == 0)
		{
			SetVelocity(Vector3(normVel.x, 1, 0) * mMuzzleSpeed);
		}
		else if (side == 1)
		{
			SetVelocity(Vector3(1, normVel.y, 0) * mMuzzleSpeed);
		}
		else if (side == 2)
		{
			SetVelocity(Vector3(-1, normVel.y, 0) * mMuzzleSpeed);
		}
		else if (side == 3)
		{
			SetVelocity(Vector3(normVel.x, -1, 0) * mMuzzleSpeed);
		}
	}
	//we'll let the cats handle the collisions
}

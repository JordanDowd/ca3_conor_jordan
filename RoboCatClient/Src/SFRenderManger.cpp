#include <RoboCatClientPCH.h>

std::unique_ptr< SFRenderManager >	SFRenderManager::sInstance;

SFRenderManager::SFRenderManager()
{
	// Might need some view stuff in here or something.
	view.reset(sf::FloatRect(0, 0, 1200, 800));
	SFWindowManager::sInstance->setView(view);
	m_lobbyScreen.setTexture(*SFTextureManager::sInstance->GetTexture("lobby_screen"));
	m_startScreen.setTexture(*SFTextureManager::sInstance->GetTexture("start_screen"));
	m_diedScreen.setTexture(*SFTextureManager::sInstance->GetTexture("died_screen"));
	m_winnerScreen.setTexture(*SFTextureManager::sInstance->GetTexture("winner_screen"));
	m_background.setTexture(*SFTextureManager::sInstance->GetTexture("background"));
	m_horizontalWall.setTexture(*SFTextureManager::sInstance->GetTexture("horizontalWall"));
	m_verticalWall.setTexture(*SFTextureManager::sInstance->GetTexture("verticalWall"));

	m_playersNeeded = 2; //set the number of players needed to be connected for the lobby screen to close
}

void SFRenderManager::RenderUI()
{
	sf::Font bebas = *FontManager::sInstance->GetFont("bebas");

	sf::Text RTT, InOut, NumberOfPlayers;

	sf::Vector2f basePos(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);

	RTT.setPosition(basePos.x + 20, basePos.y + 20);
	InOut.setPosition(basePos.x + 120, basePos.y + 20);
	NumberOfPlayers.setPosition(basePos.x + 250, basePos.y + 20);

	RTT.setFont(bebas);
	InOut.setFont(bebas);
	NumberOfPlayers.setFont(bebas);

	RTT.setCharacterSize(10);
	InOut.setCharacterSize(10);
	NumberOfPlayers.setCharacterSize(10);
	
	RTT.setFillColor(sf::Color::White);
	InOut.setFillColor(sf::Color::White);
	NumberOfPlayers.setFillColor(sf::Color::White);
	RTT.setOutlineColor(sf::Color::White);
	InOut.setOutlineColor(sf::Color::White);
	
	// RTT
	float rttMS = NetworkManagerClient::sInstance->GetAvgRoundTripTime().GetValue() * 1000.f;
	string roundTripTime = StringUtils::Sprintf("RTT %d ms", (int)rttMS);
	RTT.setString(roundTripTime);

	// Bandwidth
	string bandwidth = StringUtils::Sprintf("In %d  Bps, Out %d Bps",
		static_cast< int >(NetworkManagerClient::sInstance->GetBytesReceivedPerSecond().GetValue()),
		static_cast< int >(NetworkManagerClient::sInstance->GetBytesSentPerSecond().GetValue()));

	InOut.setString(bandwidth);

	//Number of players
	sf::Vector2f playerNum = SFRenderManager::sInstance->NumberofAliveCats();
	string players = StringUtils::Sprintf("Player Count %d",
		static_cast<int>(playerNum.y)); //x = alive, y = count
	NumberOfPlayers.setString(players);

	// Draw the text to screen.
	SFWindowManager::sInstance->draw(RTT);
	SFWindowManager::sInstance->draw(InOut);
	SFWindowManager::sInstance->draw(NumberOfPlayers);
}

void SFRenderManager::RenderShadows()
{
	sf::Vector2f player;
	if (FindCatCentre() == sf::Vector2f(-1, -1))
		player = m_lastCatPos;
	else
		player = FindCatCentre();
	auto cen = view.getCenter();
	auto size = view.getSize();

	sf::FloatRect bounds(cen.x - (size.x / 2), cen.y - (size.y / 2), size.x, size.y);
	
	// Optimization debug stuff.
	/*
	sf::FloatRect bounds(view.getCenter().x - (size.x / 2 / 2), view.getCenter().y - (size.y / 2 / 2), size.x / 2, size.y / 2);
	sf::RectangleShape r;
	r.setPosition(bounds.left, bounds.top);
	r.setSize(sf::Vector2f(bounds.width, bounds.height));
	r.setOutlineThickness(5);
	r.setFillColor(sf::Color::Transparent);
	r.setOutlineColor(sf::Color::Red);
	*/
	
	auto shadows = ShadowFactory::sInstance->getShadows(player, sf::Color::Black, bounds);
	for (auto s : shadows)
	{
		SFWindowManager::sInstance->draw(s);
	}
	//SFWindowManager::sInstance->draw(r);
}

void SFRenderManager::UpdateView()
{
	// Lower rate means more 'lag' on the camera following the player.
	float rate = .02f;
	if (FindCatCentre() != sf::Vector2f(-1, -1))
	{
		sf::Vector2f player = FindCatCentre();
		sf::Vector2f newCentre = view.getCenter() + ((player - view.getCenter()) * rate);
		view.setCenter(newCentre);
	}
	SFWindowManager::sInstance->setView(view);
}

void SFRenderManager::RenderTexturedWorld()
{
	/*for (auto spr : TexturedWorld::sInstance->getTexturedWorld())
	{
		SFWindowManager::sInstance->draw(spr);
	}*/
	SFWindowManager::sInstance->draw(m_background);
}

void SFRenderManager::RenderWalls()
{
	sf::Vector2f h1(0, 0);
	sf::Vector2f h2(0, 785);
	sf::Vector2f v1(0, 0);
	sf::Vector2f v2(1185, 0);

	m_horizontalWall.setPosition(h1);
	SFWindowManager::sInstance->draw(m_horizontalWall);

	m_horizontalWall.setPosition(h2);
	SFWindowManager::sInstance->draw(m_horizontalWall);

	m_verticalWall.setPosition(v1);
	SFWindowManager::sInstance->draw(m_verticalWall);

	m_verticalWall.setPosition(v2);
	SFWindowManager::sInstance->draw(m_verticalWall);
}

// Way of finding this clients cat, and then centre point. - Ronan
sf::Vector2f SFRenderManager::FindCatCentre()
{
	uint32_t catID = (uint32_t)'RCAT';
	for (auto obj : World::sInstance->GetGameObjects())
	{
		// Find a cat.
		if (obj->GetClassId() == catID)
		{
			RoboCat *cat = dynamic_cast<RoboCat*>(obj.get());
			auto id = cat->GetPlayerId();
			auto ourID = NetworkManagerClient::sInstance->GetPlayerId();
			if (id == ourID)
			{
				// If so grab the centre point.
				auto centre = cat->GetLocation();
				m_lastCatPos.x = centre.mX;
				m_lastCatPos.y = centre.mY;
				return sf::Vector2f(centre.mX, centre.mY);
			}
		}
	}
	return sf::Vector2f(-1, -1);
}
//using ronans code above to get the players details for the HUD
uint8_t SFRenderManager::FindCatHealth()
{
	uint32_t catID = (uint32_t)'RCAT';
	for (auto obj : World::sInstance->GetGameObjects())
	{
		// Find a cat.
		if (obj->GetClassId() == catID)
		{
			RoboCat *cat = dynamic_cast<RoboCat*>(obj.get());
			auto id = cat->GetPlayerId();
			auto ourID = NetworkManagerClient::sInstance->GetPlayerId();
			if (id == ourID)
			{
				return cat->GetHealth();
			}
		}
	}
	return 0;
}

// Returns the alive cats in the X, and the total numbers of cats in the Y.
sf::Vector2f SFRenderManager::NumberofAliveCats()
{
	int playerCount = 0;
	int aliveCats = 0;
	uint32_t catID = (uint32_t)'RCAT';
	for (auto obj : World::sInstance->GetGameObjects())
	{
		// Find a cat.
		if (obj->GetClassId() == catID)
		{
			RoboCat *cat = dynamic_cast<RoboCat*>(obj.get());
			playerCount++; //checks how many players are currently connected
			if (playerCount > m_playerCount)
			{
				m_playerCount++; //This will store the total number of players permenantly
			}
			if (cat->GetHealth() > 0)
				aliveCats++;
		}
	}
	return sf::Vector2f(aliveCats, m_playerCount);
}

void SFRenderManager::StaticInit()
{
	sInstance.reset(new SFRenderManager());
}


void SFRenderManager::AddComponent(SFSpriteComponent* inComponent)
{
	mComponents.push_back(inComponent);
}

void SFRenderManager::RemoveComponent(SFSpriteComponent* inComponent)
{
	int index = GetComponentIndex(inComponent);

	if (index != -1)
	{
		int lastIndex = mComponents.size() - 1;
		if (index != lastIndex)
		{
			mComponents[index] = mComponents[lastIndex];
		}
		mComponents.pop_back();
	}
}

int SFRenderManager::GetComponentIndex(SFSpriteComponent* inComponent) const
{
	for (int i = 0, c = mComponents.size(); i < c; ++i)
	{
		if (mComponents[i] == inComponent)
		{
			return i;
		}
	}

	return -1;
}


//this part that renders the world is really a camera-
//in a more detailed engine, we'd have a list of cameras, and then render manager would
//render the cameras in order
void SFRenderManager::RenderComponents()
{
	//Get the logical viewport so we can pass this to the SpriteComponents when it's draw time

	for (SFSpriteComponent* c : mComponents)
	{
		SFHealthSpriteComponent* ptr = dynamic_cast<SFHealthSpriteComponent*>(c);
		if (ptr)
			SFWindowManager::sInstance->draw(ptr->GetSprite());
		else
			SFWindowManager::sInstance->draw(c->GetSprite());
	}
}

void SFRenderManager::Render()
{

	// Clear the back buffer
	SFWindowManager::sInstance->clear(sf::Color::Black);

	// The game has started.
	if (mComponents.size() > 0)
	{
		// Update the view position.
		//UpdateView();

		SFRenderManager::sInstance->RenderTexturedWorld();
		/*sf::Vector2f screen(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
		m_background.setPosition(screen);
		SFWindowManager::sInstance->draw(m_background);*/

		SFRenderManager::sInstance->RenderWalls();

		SFRenderManager::sInstance->RenderComponents();

		// Draw shadows
		//RenderShadows();

		// Draw UI elements.
		SFRenderManager::sInstance->RenderUI();

		if (m_playerCount < m_playersNeeded)
		{
			sf::Vector2f basePos(view.getSize().x, view.getSize().y);

			SFWindowManager::sInstance->draw(m_lobbyScreen);
			sf::Font bebas = *FontManager::sInstance->GetFont("bebas");
			sf::Text lobbyText;

			lobbyText.setFont(bebas);
			lobbyText.setCharacterSize(60);

			//Number of players
			string players = StringUtils::Sprintf("Players Needed: %d",
				static_cast<int>(m_playerCount)); //x = alive, y = count
			lobbyText.setString(players);

			SFRenderManager::sInstance->CenterOrigin(lobbyText);

			lobbyText.setPosition(0.5f * basePos.x, 0.5f * basePos.y);

			lobbyText.setFillColor(sf::Color::White);
			//serverText.setOutlineColor(sf::Color::Red);

			// Draw the text to screen.
			SFWindowManager::sInstance->draw(lobbyText);
		}


		// Player is dead.
		if (FindCatCentre() == sf::Vector2f(-1, -1))
		{
			// Print some you are dead screen
			sf::Vector2f died(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
			m_diedScreen.setPosition(died);
			SFWindowManager::sInstance->draw(m_diedScreen);
		}
		else
		{
			// We are the last man standing.
			sf::Vector2f cats = NumberofAliveCats();

			
			if (cats.x == 1.f && FindCatHealth() > 0 && m_playerCount >= m_playersNeeded)
			{
				// Draw some you are the winner screen.
				sf::Vector2f winner(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
				m_winnerScreen.setPosition(winner);
				SFWindowManager::sInstance->draw(m_winnerScreen);
			}
		}
	}
	// The game has not started.
	else
	{
		sf::Vector2f basePos(view.getSize().x, view.getSize().y);

		SFWindowManager::sInstance->draw(m_startScreen);
		sf::Font bebas = *FontManager::sInstance->GetFont("bebas");
		sf::Text serverText, gameText;

		serverText.setFont(bebas);
		serverText.setCharacterSize(60);
		serverText.setString("Waiting for Server...");

		gameText.setFont(bebas);
		gameText.setCharacterSize(120);
		gameText.setString("Dodgeball: Ultimate");

		SFRenderManager::sInstance->CenterOrigin(serverText);
		SFRenderManager::sInstance->CenterOrigin(gameText);

		serverText.setPosition(0.5f * basePos.x , 0.5f * basePos.y);
		gameText.setPosition(0.5f * basePos.x, 0.2f * basePos.y);

		gameText.setFillColor(sf::Color::White);
		serverText.setFillColor(sf::Color::White);
		//serverText.setOutlineColor(sf::Color::Red);
		
		// Draw the text to screen.
		SFWindowManager::sInstance->draw(serverText);
		SFWindowManager::sInstance->draw(gameText);
	}
	

	// Present our back buffer to our front buffer
	SFWindowManager::sInstance->display();
}

void SFRenderManager::CenterOrigin(sf::Text& text)
{
	sf::FloatRect bounds = text.getLocalBounds();
	text.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
}
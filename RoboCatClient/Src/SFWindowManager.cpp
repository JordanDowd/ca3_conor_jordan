#include <RoboCatClientPCH.h>

std::unique_ptr<sf::RenderWindow> SFWindowManager::sInstance;

bool SFWindowManager::StaticInit()
{
	sInstance.reset(new sf::RenderWindow(sf::VideoMode(1200, 800), "Dodgeball: ULTIMATE"));
	return true;
}